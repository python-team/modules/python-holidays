#!/usr/bin/python3

import holidays
import sys

us_holidays = holidays.US()
sys.exit(us_holidays.get('2014-01-01') != "New Year's Day")
